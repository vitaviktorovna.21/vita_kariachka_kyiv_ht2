using System;
using System.Linq;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace LatestTest
{
    public class MainPageTest
    {
        IWebDriver driver;

        readonly By _signInButton = By.XPath("//div[@class='header-bottom__login flex-wrap middle-xs']//div[@class='header-bottom__right-icon']");
        readonly By _loginInputButton = By.XPath("//span[text()='����� �������� ��� email']//following-sibling::div/input[@type='text'][@name='login']");
        readonly By _passwordInputButton = By.XPath("//input[@class='validate show-password']");
        readonly By _enterButton = By.XPath("//button[@class='button-reset main-btn submit main-btn--green']");
        readonly By _userLogin = By.XPath("//div[@class='col-xs-12 js_message']");
        


        const string _login = "v-i-tt-a@mail.ru";
        const string _password = "baci1us_anthraX";
        const string _expectedLogin = "�� ������ ��������������!";

        [SetUp]
        public void Setup()
        {
            driver = new ChromeDriver();
            driver.Manage().Window.Maximize();
            driver.Navigate().GoToUrl("https://avic.ua/");
        }

        [Test]
        public void CheckAvtorization()
        {
            var signIn = driver.FindElement(_signInButton);
            signIn.Click();
            var login = driver.FindElement(_loginInputButton);
            login.SendKeys(_login);
            var password = driver.FindElement(_passwordInputButton);
            password.SendKeys(_password);
            var enter = driver.FindElement(_enterButton);
            enter.Click();
            var actualLogin = driver.FindElement(_userLogin).Text;
            Assert.AreEqual(_expectedLogin, actualLogin);
        }

       
            
        [Test]
        public void CheckThatUrlContainsSearchWord()
        {
            driver.FindElement(By.XPath("//input[@id='input_search']")).SendKeys("iphone 13");
            driver.FindElement(By.XPath("//button[@class='button-reset search-btn']")).Click();
            Assert.IsTrue(driver.Url.Contains("query=iphone"));
        }
        [Test]
        public void GadgetPageShouldContainsCorrectData()
        {
            // Arrange
            var expected = "������ �������";

            // Act
            driver.FindElement(By.XPath("//span[@class='sidebar-item']")).Click();
            driver.FindElement(By.XPath("//main/section[1]//div/div/ul/li[8]")).Click();

            // Assert
            var result = driver.FindElement(By.XPath("//*[@id='mCSB_1_container']/div/h2")).Text;
            Assert.AreEqual(expected, result);
        }
        [Test]
        public void CheckElementsAmountOnSearchPage()
        {
            driver.FindElement(By.XPath("//input[@id='input_search']")).SendKeys("�������");
            driver.FindElement(By.XPath("//button[@class='button-reset search-btn']")).Click();
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(20);
            
            var elementsList = driver.FindElements(By.XPath("//div[@class='prod-cart height']"));
            var actualElementsSize = elementsList.Count();

            Assert.AreEqual(actualElementsSize, 12);
        }
        


        [TearDown]
        public void TearDown()
        {
            driver.Quit();
        }
    }
}